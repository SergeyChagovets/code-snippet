<?php

namespace Alser\QuickOrder\Model;

class Quote extends \Magento\Quote\Model\Quote
{
    /**
     * Retrieve item model object by item identifier
     *
     * @param   int $itemId
     * @return  \Magento\Quote\Model\Quote\Item | false
     */
    public function getItemById($itemId)
    {
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getId() == $itemId) {
                return $item;
            }
        }

        return false;
    }
}