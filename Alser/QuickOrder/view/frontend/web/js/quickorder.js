define(
    ['jquery'],
    function($){
        'use strict';
        $.widget('alser.quickorder', {
            options: {
                increase: $('.btn--plus'),
                decrease: $('.btn--minus'),
                cityselect: $('#location-top-list span')
            },

            _create: function() {
                this.options.increase.on('click', function() {
                    var val = $("#quickQty").val();
                    $("#quickQty").val(parseInt(val) + 1);
                });

                this.options.decrease.on('click', function() {
                    var val = $("#quickQty").val();
                    if (parseInt(val) > 1) {
                        $("#quickQty").val(parseInt(val) - 1);
                    }
                });

                $("#cityvalue").val(this.options.cityselect.text());
                this.options.cityselect.on('DOMSubtreeModified', function () {
                    $("#cityvalue").val($('#location-top-list span').text());
                });
            },
        });

        return $.alser.quickorder;
});

    
