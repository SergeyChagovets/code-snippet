<?php

namespace Alser\QuickOrder\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     *  Quickorder default name
     */
    const QUICKORDER_DEFAULT_NAME = 'quickorder';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quote;

    /**
     * @var \Magento\Quote\Model\QuoteManagement
     */
    protected $_quoteManagement;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @param \Magento\Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_quote = $quote;
        $this->_quoteManagement = $quoteManagement;
        $this->_customerFactory = $customerFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $postData = $this->getRequest()->getParams();

        $city = '-';
        if(array_key_exists('city', $postData)){
            $city = $postData['city'];
        }

        $firstname = $postData['firstname'];
        $lastname = self::QUICKORDER_DEFAULT_NAME;

        $store = $this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer = $this->_customerSession->getCustomer();
        $quote = $this->_quote->create();
        
        if (!$customer->getId()){
            $customer = $this->_customerFactory->create();
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($firstname)
                ->setLastname($lastname)
                ->setEmail('quickorder@gmail.com');
            $quote->setCheckoutMethod('guest');
            $quote->setCustomerId(null);
            $quote->setCustomerEmail('quickorder@gmail.com');
            $quote->setCustomerIsGuest(true);
            $quote->setCustomerGroupId(\Magento\Customer\Api\Data\GroupInterface::NOT_LOGGED_IN_ID);
        } else {
            $quote->assignCustomer($customer);
        }
        
        $quote->setStore($store);
        $quote->setCurrency();
        $quote->setOrderType('quickorder');

        $product = $this->_product->load($postData['productId']);
        $quote->addProduct(
            $product,
            intval($postData['qty'])
        );

        //Set Address to quote
        $shipAddress = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'street' => '-',
            'city' => $city,
            'country_id' => 'KZ', //Kazakhstan
            'region' => '-',
            'postcode' => '111111', //postcode can be any
            'telephone' => $postData['phone'],
            'save_in_address_book' => 0,
        );
        $quote->getBillingAddress()->addData($shipAddress);
        $quote->getShippingAddress()->addData($shipAddress);
        
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('freeshipping_freeshipping'); //use free shipping
        $quote->setPaymentMethod('checkmo'); //use check-money order
        $quote->setInventoryProcessed(false);
        $quote->save();

        $quote->getPayment()->importData(['method' => 'checkmo']);

        $quote->collectTotals()->save();

        try {
            $order = $this->_quoteManagement->submit($quote);

            $order->setEmailSent(0);
            if($order->getEntityId()) {
                $this->messageManager->addSuccess(__('Your order # is: %1', $order->getRealOrderId()));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Something went wrong'));
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }
}